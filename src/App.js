import React, { useState, useEffect } from 'react';
import {Route} from 'react-router-dom';
import axios from 'axios'; 
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import NavBar from './Components/NavBar';
import Garde from './Components/Garde';
import PharmaList from './Components/PharmaList';
import AddPharma from './Components/AddPharma';
import Header from './Components/Layouts/Header';
import Footer from './Components/Layouts/Footer';

function App() {
    const [lists, setLists] = useState([])
    useEffect(() => {
      axios.get(`https://cherry-tart-07032.herokuapp.com/pharma`)
      .then(res => setLists(res.data))
      .catch(error => console.log(error));
    }, []);

    return (
      <div className="App">
        <Header />
        <NavBar />
        <Route exact path="/" render={() => <PharmaList lists={lists}/>} />
        <Route path="/add-pharma" component={AddPharma} />
        <Route path="/garde" component={Garde} />
        <Footer />
      </div>
    );
}

export default App;

//<Route path="/update/:id" render={props => <ModifyPharma {...props} lists={lists}/>} />