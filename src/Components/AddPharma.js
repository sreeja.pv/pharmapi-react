import React, {useState} from 'react';
import axios from 'axios';
import styled from 'styled-components';
//import './App.css';

const AddPharma = () => {
const [nom, setNom] = useState("");
const [quartier, setQuartier] = useState("");
const [ville, setVille] = useState("");
const [garde, setGarde] = useState("");
const [message, setMessage] = useState('');

const handleSubmit = e => {
    e.preventDefault();

    const pharmacy = {
        nom,
        quartier,
        ville,
        garde
    }
    
    setNom("");
    setQuartier("");
    setVille("");
    setGarde("");

    axios.post(`https://cherry-tart-07032.herokuapp.com/pharma`, pharmacy)
        .then(res => setMessage(res.data))
        .catch(err => {
            console.log(err);
        })
}
    return (
        <AddPharmaContainer>
        <div class='container'>
            <h1>Add Pharmacy</h1>
            <span className='message'>{message}</span>
            <form onSubmit={handleSubmit} encType="multipart/form-data">
                <div className="form-group">
                    <label htmlFor="nom">Nom: </label>
                    <input type="text" value={nom} onChange={e => setNom(e.target.value)} className="form-control" placeholder="Enter nom" />
                </div>
                <div className="form-group">
                    <label htmlFor="quartier">Quartier</label>
                    <input type="text" value={quartier}onChange={e => setQuartier(e.target.value)} className="form-control" placeholder="quartier" />
                </div>
                <div className="form-group">
                    <label htmlFor="ville">Ville</label>
                    <input type="text" value={ville} onChange={e => setVille(e.target.value)} className="form-control" placeholder="ville" />
                </div>
                <div className="form-group">
                    <label htmlFor="garde">Garde</label>
                    <select className="form-control" value={garde} onChange={e => setGarde(e.target.value)}>
                    <option>Choose.....</option>
                    <option>Lundi</option>
                    <option>Mardi</option>
                    <option>Mercredi</option>
                    <option>Jeudi</option>
                    <option>Vendredi</option>
                    <option>Samedi</option>
                    <option>Dimanche</option>
                    </select>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        </div>
        </AddPharmaContainer>
    )
}

export default AddPharma;

// Main add container

const AddPharmaContainer = styled.div`
    // margin: 0 auto;
    // padding 2rem;
    // width: 32.25rem;

    h1 {
        font-weight: 900;
        color: var(--light-green);
    }

    .btn-primary {
        margin-top: 2rem;
        background: var(--dark-green);
        border: none;
        &:hover {
            background: var(--dark-green);
        } 
    }
    .message {
        font-weight: 900;
        color: tomato;
        padding: 1rem 1rem 1rem 0;
    }
`;