import React from 'react'
import styled from 'styled-components';

const Header = () => {
    return <MainContainer><h1>Pharmacy with react</h1></MainContainer>;
};

export default Header

//Main Container
const MainContainer = styled.header`
    background: url(../../images/header-bg.jpg) no-repeat center/cover;
    height: 10rem;

    h1{
        transform: translate(-50%, -50%);
        color: #fff;
        font-weight; 900;
        position: absolute;
        top: 15%;
        left: 50%;
    }
    `;
//https://www.youtube.com/watch?v=wTs6Oa4m47o
