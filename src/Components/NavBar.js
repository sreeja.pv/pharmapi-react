import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';

class NavBar extends React.Component {
    
    render() {
        return (
            <NavbarContainer>
            <nav className="navbar navbar-expand-lg navbar-light px-5 py-0">
                <Link className="navbar-brand" href="#">PHARMA</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto">
                    <li className="nav-item active">
                        <Link className="nav-link" to="/">Pharmacies <span className="sr-only">(current)</span></Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/garde">Garde</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/add-pharma">Ajoute une pharmacie</Link>
                    </li>
                    </ul>
                </div>
            </nav>
            </NavbarContainer>
        )
    }
}

export default NavBar;

//Main navbar container

const NavbarContainer = styled.div `
    background: var(--dark-green);
    .nav-link {
        color: #000!important;
        &:hover  {
            background: var(--light-green);
        }
    }
`;