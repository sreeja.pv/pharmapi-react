import React from 'react';
import axios from 'axios';
//import './App.css';

export default class Garde extends React.Component {
    state = {
      pharmas: []
    }
    
    componentDidMount(){
      axios.get(`http://127.0.0.1:8000/pharma-garde`)
      .then(res => {
        console.log(res);
        this.setState({pharmas: res.data});
      });
    }

    edit(){
      this.setState({
        message: 'would you like to edit?'
      })
    }

    render() {
      return (
        <div>
          <ul>
            { 
              this.state.pharmas.map(pharma => 
              <li>{pharma.garde}</li>)
            }
          </ul>
        </div>
      )
    }
}