import React, { useState } from 'react';
import axios from 'axios';
import ModifyPharma from './ModifyPharma';

const PharmaList = ({ lists }) => {
  const [pharma, setPharma] = useState([]);
  const [editPharma, setEditPharma] = useState({});
  const [showPharma, setShowPharma] = useState(false);

  // Delete pharma

  const deletePharma = id => {
    //console.log(id)
    axios.delete(`https://cherry-tart-07032.herokuapp.com/pharma/${id}`)
      .then(res => alert(res.data));
    setPharma(pharma.filter(elem => elem.id !== id));
  }

  // Update pharma

  const updatePharma = (pharma, showPharma) => {
    console.log(pharma);
    setEditPharma(pharma);
    setShowPharma(true);
  };

  return (
    <div className='container'>
      {showPharma && <ModifyPharma pharma={editPharma}  />}
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Nom</th>
            <th scope="col">Quartier</th>
            <th scope="col">Ville</th>
            <th scope="col">Garde</th>
          </tr>
        </thead>
        <tbody>
          {lists.map((pharma, key) => (
            <tr key={key}>
              <td>{pharma.id}</td>
              <td>{pharma.nom}</td>
              <td>{pharma.quartier}</td>
              <td>{pharma.ville}</td>
              <td>{pharma.garde}</td>
              <td><button className='btn btn-outline-success' onClick={() => updatePharma(pharma, showPharma)}>Edit</button></td>
              <td><button className='btn btn-outline-danger' onClick={() => deletePharma(pharma.id)}>Delete</button></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default PharmaList
